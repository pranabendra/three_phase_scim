import numpy
import pandas
import bisect
import math

df_swg = pandas.read_excel('.\\lookupTables\\SWG.xlsx')
rect_swg = pandas.read_excel('.\\lookupTables\\RectangularCopperConductor.xlsx', header=None)
swg_matrix = df_swg.values
rect_matrix = rect_swg.values
#print(swg_matrix)

area_given_by_user = 80

swg_column = swg_matrix[:,0]
diameter_column = swg_matrix[:,1]
area_column = swg_matrix[:,2]

swg_index = bisect.bisect_right(area_column, area_given_by_user)

swg_number = swg_column[swg_index]
area_to_be_used = area_column[swg_index]
diameter_to_be_used = diameter_column[swg_index]

print(swg_number, area_to_be_used, diameter_to_be_used)

# print(rect_matrix)
# print(rect_matrix.shape)
all_possible_areas = []
all_possible_dimensions = []
for width_index in range(1, 33):
    for thickness_index in range(1, 31):
        if rect_matrix[width_index][thickness_index] == -1:
            continue
        else:
            if rect_matrix[width_index][thickness_index] > area_given_by_user:
                all_possible_areas.append(rect_matrix[width_index][thickness_index])
                all_possible_areas.append(rect_matrix[width_index][thickness_index])
                all_possible_dimensions.append((rect_matrix[width_index][0], rect_matrix[0][thickness_index]))
                all_possible_dimensions.append((rect_matrix[0][thickness_index], rect_matrix[width_index][0]))
                break

print(all_possible_areas)
print(all_possible_dimensions)
