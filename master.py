import rating
import loadTable
from classes.metric import Length, Area
from mainDimensions.mainDimensions import compute_main_dimension
from statorDesign.statorWinding import stator_design_winding
from statorDesign.adjustment import adjust_initial_values
from statorDesign.statorConductorSlot import stator_conductor_size_and_slot
from statorDesign.statorCore import stator_core
from rotorDesign.airGap import air_gap
from rotorDesign.rotorSlot import rotor_slots
from rotorDesign.endRings import end_rings_rotor
from rotorDesign.rotorCore import rotor_core
from noLoadCurrent.magAirGap import mag_air_gap
from noLoadCurrent.magStatorTeeth import mag_stator_teeth
from noLoadCurrent.magStatorCore import mag_stator_core
from noLoadCurrent.magRotorTeeth import mag_rotor_teeth
from noLoadCurrent.magRotorCore import mag_rotor_core
from noLoadCurrent.magNoLoad import mag_no_load
from noLoadCurrent.lossStatorTeeth import loss_stator_teeth
from noLoadCurrent.lossStatorCore import loss_stator_core
from noLoadCurrent.noLoadCurrent import no_load_current
from shortCircuitCurrent.statorSlotLeakage import stator_slot_leakage
from shortCircuitCurrent.rotorSlotLeakage import rotor_slot_leakage
from shortCircuitCurrent.slotLeakageReactance import slot_leakage_reactance
from shortCircuitCurrent.overhangLeakage import overhang_leakage
from shortCircuitCurrent.totalLeakage import total_leakage
from shortCircuitCurrent.Resistance import resistance
from shortCircuitCurrent.impedanceLossesEfficiency import Z_losses_eff
from temperatureRise.temperatureRise import temperature_rise

swg_matrix, rect_matrix, carter_matrix, lohys_bat_matrix, fwloss_matrix, lohys_loss_matrix, air_gap_matrix, slot_leakage_matrix = loadTable.load_table()
print()
print()
print('Hello!')
print()
print()

# Variables
Bav = 0.42
Kw = 0.955
ac = 20000
qs = 2
L_by_tau = 1.6
delta = 6
space_factor = 0.4

count = 0
currentCriterion = False
while 1 - (currentCriterion  == True and Bav >= 0.35):
    Bav = 0.42
    # Enter Bav, ac, Kw, L/tau ratio
    D, L, tau, Li, isDuct = compute_main_dimension(Bav, ac, Kw, L_by_tau)
    # D, L, tau, Li, isDuct = compute_main_dimension(0.65, 20000, 0.95, 1.5)
    print('===============================================================')
    #print(D.m, L.m, Li.m, tau.m, isDuct)

    # Continue from here
    phi, Ts, Ss, yss, Zss, Kws, Cs = stator_design_winding(Bav, tau.m, L.m, D.m, Kw, qs)

    # phi, Ts, Ss, yss, Zss, Kws = stator_design_winding(0.65, 0.112, 0.167, 0.214, 0.95, 2)

    print(phi, Ts, Ss, yss.mm, Zss, Kws)

    phi, Ts, Ss, Zss, Bav, ac = adjust_initial_values(Zss, Ss, qs, Bav, ac, tau, L, Kw)

    Is, W2, Wsss, dss, Wts, Lmts, isCircular, h4, h3, h1, Bts, As = stator_conductor_size_and_slot(phi, Ss, rating.poles, D, L, Li, tau, delta, Zss, space_factor, rect_matrix, swg_matrix)

    Acs, dcs, D0, Bcs = stator_core(phi, Li, D, dss)

    print('Stator Design Completed')
    print('===============================================================')

    lg, Dr = air_gap(L, D, air_gap_matrix)

    Sr, Ib, rotor_bar_area, Wsr, dsr, Btr_13, rotor_copper_loss, Lb, ysr, W0, Wsrr, h1, h3, h4, noAreaFound = rotor_slots(Ss, Kws, Ts, Is, Dr, rect_matrix, phi, Li, L)

    total_rotor_copper_loss, slip = end_rings_rotor(Sr, Ib, Dr, dsr, rotor_copper_loss)

    dcr, Bcr, Di = rotor_core(Dr, dcs, dsr, Bcs)

    print('Rotor Design Completed')
    print('===============================================================')

    s_W0 = Length(0.002)
    r_W0 = Length(0.0015)
    ATg = mag_air_gap(s_W0, r_W0, lg, yss, ysr, isDuct, rating.duct_width, L, D, 1, Bav, carter_matrix)
    ATs = mag_stator_teeth(Ss, Wts, Li, Bts, dss, lohys_bat_matrix)
    ATcs = mag_stator_core(D, Li, dcs, dss, Bcs, lohys_bat_matrix)
    ATr = mag_rotor_teeth(Btr_13, dsr, lohys_bat_matrix)
    ATcr = mag_rotor_core(Bcr, Li, Dr, dsr, dcs, dcr, lohys_bat_matrix)
    Im = mag_no_load(Kws, Ts, ATg, ATs, ATcs, ATcr, ATr)
    loss_st = loss_stator_teeth(Ss, Wts, Li, dss, Bts, lohys_loss_matrix)
    loss_sc = loss_stator_core(D0, D, dss, Li, Bcs, lohys_loss_matrix)
    I0, phase_angle, currentCriterion, total_noload_loss, fw_loss, iron_loss = no_load_current(loss_st, loss_sc, Im, Is, fwloss_matrix)

    print('No Load Current Completed')
    print('===============================================================')

    slot_permeance_stator = stator_slot_leakage(Wsss, W2, W0, isCircular, h1, h3, h4)
    slot_permeance_rotor = rotor_slot_leakage(Wsss, W0, h1, h3, h4, Kws, Ss, Sr)
    X0 = overhang_leakage(yss, tau, Cs, Ss, Ts, qs, slot_leakage_matrix)
    xs = slot_leakage_reactance(slot_permeance_stator, slot_permeance_rotor, Ts, L, qs)
    Xs = total_leakage(Im, Ss, Sr, xs, X0)
    Rs, total_stator_copper_loss = resistance(Ts, As, Lmts, Is, total_rotor_copper_loss)
    Z, Isc, pf_sc, phase_sc, loss_fl, eff = Z_losses_eff(Rs, Xs, total_stator_copper_loss, total_rotor_copper_loss, total_noload_loss)

    print('Short Circuit Current Completed')
    print('===============================================================')

    theta = temperature_rise(iron_loss, total_stator_copper_loss, L, Lmts, D, D0, 80)

    print('Temperature Rise Completed')
    print('===============================================================')

    print("Bav                      : ", round(Bav,3))
    print("AC                       : ", ac)
    print("Efficiency (expected)    : ", 100*rating.efficiency)
    print("Efficiency (obtained)    : ", round(eff,1))
    print("Temperature Rise (degC)  : ", round(theta))
    print("Current Criterion        : ", currentCriterion)
    if ac >= 18000 and ac <= 24000 and currentCriterion and Bav >= 0.35 and Bav <= 0.6:
        break
    else:
        count += 10
        # print("Exit count: ", count)
        if count >= 6000:
            print("EXIT")
            if qs == 3:
                if ac >= 24000 and ac <= 18000:
                    count = 0
                    continue
                else:
                    break 
            if qs == 2:
                qs = 3
                count = 0 
        else:
            ac = 18000 + count
            # print(ac, Bav)
